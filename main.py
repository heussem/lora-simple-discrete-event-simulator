from transmitevent import *
from math import *
from random import random, uniform
import matplotlib.pyplot as plt
import argparse
from statistics import mean

#db-related functions
from util import *

nb_events=10000* 1000
#Aloha sim nb nodes
aloha_sim_nb_nodes=[25,50,100,150,200,300,400,600]

# These variables will be set according to the 1st argument
R=0
node_density=0
sf_radii=[]

sf7_eqpwr=False
same_dc=False

#does not return anything if out of range
def sf_of_d(d):
    prev_r=0
    for i in (range(len(sf_radii)-1)):
        sf = 7+i
        if(d > prev_r and d<sf_radii[i]): return(sf)
        else: prev_r=sf_radii[i]
    return(12)

thN= -174+6+db(125e3) #dBm

toa = {7:102.7e-3, 8:184.8e-3, 9:0.3287, 10:0.6165, 11:1.315, 12:2.466}
q_sf= {7:-6, 8:-9, 9:-12, 10:-15, 11:-17.5, 12:-20}
#if I want a signal at SF8 and there is interference at SF7, it would need to be 24dBs more to hurt me
rej_below = {7:0, 8: 24, 9:27, 10: 30, 11: 33, 12:36}
#if I want a signal at SF8 and there is interference at SF11, it would need to be 20dBs more to hurt me (this is more demanding than matrix in Goursaud&Gorce)
rej_above = {7: 16, 8:20, 9:23, 10:26, 11:29, 12:0}
capt_margin=6 #dB
#Transmit pwr (dBm)
tp=14
#GW antenna height (in m) (used by Okumara Hata)
hb=15
antenna_gain=6 #dB

#Traffic descr.
intens=1./(toa[12]*300) # longest transmission (SF12), 1% duty cycle, 3 freq. channels


def next_date(t_prev):
    nb=node_density * pi * (R/1000)**2
    return next_date_nb(nb,t_prev)

def next_date_nb(nb,t_prev):
    # next transmit event depends on individual intensity and nb nodes
    return t_prev-log(random()) / intens /nb

def nodes_in_sf(sf):
    dist_t=sf_radii.copy()
    dist_t[12-7]=R/1000
    dist_t.insert(0,0)
    return node_density * pi * ((dist_t[sf-7+1])**2 - (dist_t[sf-7])**2)

def next_date_sf(t_prev,prev_sf,time_sf_t): #returns time,time_sf_t,distance,sf where time_sf_t is a tab of next trans. dates in each SF
    # next transmit event depends on individual intensity in this SF, and nb nodes
    time_next=t_prev
    next_sf=prev_sf
    while time_next==t_prev :
        nb_sf=nodes_in_sf(next_sf)
        traffic_intens_sf=1/(toa[next_sf]*300)
        time_sf_t[next_sf]=time_sf_t[next_sf]-log(random())  /nb_sf / traffic_intens_sf
        next_sf=min(time_sf_t,key=time_sf_t.get)
        time_next=time_sf_t[next_sf]
        low_r=0 if next_sf==7 else (sf_radii[next_sf-7-1]*1000/R)**2
        hig_r=(sf_radii[next_sf-7]*1000/R)**2 if next_sf != 12 else 1
        distance_next=R*sqrt(uniform(low_r,hig_r))
    return time_next, time_sf_t, distance_next, next_sf

#Returns att in dB
def okumara_hata(distance):
    _d=distance/1000
    return 69.55+26.16*log10(868)-13.82*log10(hb)  \
        +(44.9-6.55*log10(hb))*log10(_d)-2*(log10(868/28))**2-5.4

#Returns small scale fading in dB
def rayleigh_gain():
    ssf=-log(random())
    return db(ssf)

### Useful functions for stats
def time_deltas(tab):
    l=len(tab)
    return list(map(lambda x,y: x.time-y.time,tab[1:l-1],tab[0:l-2]))

def get_sf(tab):
    return list(map(lambda x: x.sf,tab))

def get_r_pwr(tab):
    return list(map(lambda x: x.r_pwr,tab))

def get_distances(tab):
    return list(map(lambda x: x.distance,tab))

def plot_toa_distances(tab):
    r_bins=200
    d_t=[]
    for i in range(r_bins):
        d_t.append(0)
    for i in range(len(tab)):
        r_bin=floor(tab[i].distance/R*r_bins)
        d_t[r_bin]+=1
    #divide by toa
    for i in range(r_bins):
        # We expect to have a number of transmissions inv. prop. to the toa
        d_t[i]=d_t[i]*toa[sf_of_d(R/1000/r_bins*i)]
    plt.plot(d_t)
    plt.show()

def plot_sf_nb(tab):
    r_bins=200
    nb_t=[]
    for i in range(7,13):
        nb_t.append(0)
    for i in range(len(tab)):
        nb_t[tab[i].sf-7]+=1
    sim_duration=tab[len(tab)-1].time
    for i in range(7,13):
        nb_t[i-7]=nb_t[i-7]/nodes_in_sf(i)/sim_duration*toa[i]/toa[12]
    plt.plot(nb_t,'bo')
    plt.show()

def get_duration(tab):
    return list(map(lambda x: x.duration,tab))

def nb_ok(tab):
    tot_ok=0
    for i in range(len(tab)):
        tot_ok+=tab[i].decoded*(tab[i].coll==0)
    return tot_ok

def nb_decoded(tab):
    return sum(list(map(lambda x: x.decoded,tab)))

def ok_vs_dist(tab):
    r_bins=100
    dists=[]
    r_pwr=[]
    ok_tab=[]
    ok_inter_tab=[];
    inter_sf=[];
    inter_below=[];
    nb_tab=[]
    for i in range(r_bins):
        dists.append(R*sqrt((i+0.5)/r_bins))
        r_pwr.append(0)
        ok_tab.append(0)
        ok_inter_tab.append(0)
        inter_sf.append(0)
        inter_below.append(0)
        nb_tab.append(0)

    for i in range(len(tab)):
        r_bin=floor((tab[i].distance/R)**2*r_bins)
        r_pwr[r_bin]+=tab[i].r_pwr
        nb_tab[r_bin]+=1
        ok_tab[r_bin]+=tab[i].decoded*(tab[i].coll==0)
        ok_inter_tab[r_bin]+=tab[i].decoded*(tab[i].inter_sf_coll==0)*(tab[i].coll==0)
        inter_sf[r_bin]+=(tab[i].inter_sf_coll!=0)
        inter_below[r_bin]+=(tab[i].inter_sf_below!=0)
    #Normalize in place
    for i in range(r_bins):
        if(nb_tab[i]>0):
            ok_tab[i]=ok_tab[i]/nb_tab[i]
            r_pwr[i]=r_pwr[i]/nb_tab[i]
            if inter_sf[i]>0:
                inter_below[i]=inter_below[i]/inter_sf[i]
            else: inter_below[i]=0
            ok_inter_tab[i]=ok_inter_tab[i]/nb_tab[i]

        else: ok_tab[i]=0;ok_inter_tab[i]=0
    return dists,ok_tab,ok_inter_tab,inter_below,r_pwr


def nb_ok_1(tab):
    tot_ok=0
    for i in range(len(tab)):
        tot_ok+=tab[i].decoded*(tab[i].coll==0)*(tab[i].all_coll<=1)
    return tot_ok

def nb_ok_0(tab):
    tot_ok=0
    for i in range(len(tab)):
        tot_ok+=tab[i].decoded*(tab[i].all_coll==0)
    return tot_ok

def list_1_coll(tab):
    return list(filter(lambda x :(x.all_coll==1),tab))


def init_sim_homogeneous(nb,t_a, e_a):
    time=0
    for i in range(nb_events):
        time=next_date_nb(nb,time)
#         sf=int(7+random()*6)
        sf=12
        duration=toa[sf]
        n_e=AnnotatedTransmitEvent(time,duration,R,sf)
        e_e=EndOfTrans(time+duration,i)
        #large scale fading
        n_e.r_pwr=tp-okumara_hata(n_e.distance)+antenna_gain
        #small scale fading
        n_e.ss_fading=rayleigh_gain()
        n_e.r_pwr+=n_e.ss_fading
        #does it overcome thermal noise?
        if(n_e.r_pwr-thN>q_sf[sf]): n_e.decoded=1
        else: n_e.decoded=0
        t_a.append(n_e)
        e_a.append(e_e)
    print("Sorting end events")
    e_a.sort(key=lambda e_e:e_e.time) # should not be necessary?

def init_sim_all_sf(t_a, e_a):
    time=0
    time_sf={7:0,8:0,9:0,10:0,11:0,12:0}; sf=7 # use when same_dc=True
    for i in range(nb_events):
        if not same_dc:
            time=next_date(time)
            distance=R*sqrt(random())
            sf=sf_of_d(distance/1000)
        else:
            time,time_sf,distance,sf=next_date_sf(time,sf,time_sf)
        duration=toa[sf]
        n_e=AnnotatedTransmitEvent(time,duration,distance,sf)
        e_e=EndOfTrans(time+duration,i)
        #large scale fading
        n_e.r_pwr=tp-okumara_hata(n_e.distance)+antenna_gain
        if(sf7_eqpwr and sf==7):
            att_advantage=okumara_hata(sf_radii[0]*1000)-okumara_hata(n_e.distance) #eg 12.4dB
            att_advantage_steps=int((att_advantage-2)/2) #eg 4
            att_advantage_steps=min(att_advantage_steps,6) # will remove at most 12dB (2dBm PTx, ADR TXPower = 5)
            n_e.r_pwr=tp-okumara_hata(n_e.distance)-2*att_advantage_steps+antenna_gain
        #small scale fading
        n_e.ss_fading=rayleigh_gain()
        n_e.r_pwr+=n_e.ss_fading
        #does it overcome thermal noise?
        if(n_e.r_pwr-thN>q_sf[sf]): n_e.decoded=1
        else: n_e.decoded=0
        t_a.append(n_e)
        e_a.append(e_e)
    print("Sorting end events")
    e_a.sort(key=lambda e_e:e_e.time)

def print_progress(i,l):
    if not(i%(l/10)): print(str(i/l*100)+"%")

def check_ended(cur_time,k,t_a,e_a,inter_level,ongoing_frames):
    # Check all finished events. For each, reduce inter_level
    while(e_a[k].time<cur_time):
        matching_t_a=t_a[e_a[k].ref]
        inter_level[matching_t_a.sf]-=inv_db(matching_t_a.r_pwr)
        ongoing_frames[matching_t_a.sf]-=1
        k+=1
    return k

def apply_inter(t_a,ind,inter_level,nb_ongoing_frames): #inter_level not in dB!
    sf_i=t_a[ind].sf
    pwr_i=t_a[ind].r_pwr
    # First look at co-SF interference
    cur_interference_level = inter_level[sf_i]-inv_db(pwr_i)
    if(cur_interference_level > t_a[ind].co_sf_interference_level):
        t_a[ind].co_sf_interference_level=cur_interference_level
    if(cur_interference_level>0):
        if (inv_db(pwr_i-capt_margin) < cur_interference_level):
            t_a[ind].coll+=nb_ongoing_frames
        t_a[ind].all_coll+=nb_ongoing_frames
    # Inter-SF from lower SF
    sum_inter_below=0;sum_inter_above=0
    for i in range(7,12):
        if i<sf_i:sum_inter_below+=inter_level[i]
        elif i>sf_i:sum_inter_above+=inter_level[i]
    if sum_inter_below > inv_db(pwr_i+rej_below[sf_i]) or sum_inter_above > inv_db(pwr_i+rej_above[sf_i]):
        t_a[ind].inter_sf_coll+=1
    if sum_inter_below > inv_db(pwr_i+rej_below[sf_i]):
        t_a[ind].inter_sf_below+=1

def simulate(t_a, e_a):
    k=0 # index in array of end times
    inter_level={7:0.0, 8:0.0, 9:0.0, 10:0.0, 11:0.0, 12:0.0}
    ongoing_frames={7:0.0, 8:0.0, 9:0.0, 10:0.0, 11:0.0, 12:0.0}
    for i in range(len(t_a)):
        print_progress(i,len(t_a))
        cur_time=t_a[i].time
        k=check_ended(cur_time,k,t_a,e_a,inter_level,ongoing_frames)
        inter_level[t_a[i].sf]+=inv_db(t_a[i].r_pwr)
        ongoing_frames[t_a[i].sf]+=1
        #See if this transmission survives the level of interference when it starts
        # NB : the interference level includes the pwr from the transmission itself
        if(ongoing_frames[t_a[i].sf]>1):
            apply_inter(t_a,i,inter_level,ongoing_frames[t_a[i].sf]-1)
        # This level on interference applies to all transmissions that it's going to intersect.
        l=0;cont=1 #impacte le bon nombre de trames
        while(k+l < len(e_a) and cont==1):
            if e_a[k+l].time< cur_time+t_a[i].duration:
                if e_a[k+l].ref<i:
                    apply_inter(t_a,e_a[k+l].ref,inter_level,1)
                l+=1
            else: cont=0

def output_stats_homogeneous(t_a):
    # trim 5% at both ends
    del t_a[:int(nb_events*0.05)]
    del t_a[int(nb_events*0.90):]
    nb_events_stats=len(t_a)
    success_ratio=nb_ok(t_a)/nb_events_stats
    success_ratio_1=nb_ok_1(t_a)/nb_events_stats
    success_ratio_0=nb_ok_0(t_a)/nb_events_stats
    timespan=t_a[nb_events_stats-1].time+t_a[nb_events_stats-1].duration-t_a[0].time
    load_ratio=sum(get_duration(t_a))/timespan
    utilization=success_ratio*load_ratio
    out_file=open("aloha-"+str(R)+".csv","a+")
    print(str(load_ratio) +", "+ str(utilization) +", "+str(success_ratio)+", "+ str(success_ratio_1*load_ratio)+", "+ str(success_ratio_0*load_ratio), file=out_file)

#     coll_inter_1=list(map(lambda x: inv_db(db(x.co_sf_interference_level)+okumara_hata(x.distance)-antenna_gain-tp),list_1_coll(t_a)))
#     plt.hist(coll_inter_1, bins=30)
#     gain_inter_1=list(map(lambda x: inv_db(x.r_pwr+okumara_hata(x.distance)-antenna_gain-tp),list_1_coll(t_a)))
#     plt.hist(gain_inter_1, bins=30)
#     print(mean(coll_inter_1))
#     print(len(coll_inter_1)/len(t_a))
#     plt.show()

opt_string=""

def output_stats_all_sf(t_a):
    dt,ot,oit,ib,pt =ok_vs_dist(t_a)
#     plt.plot(dt,ot)
#     plt.hist(get_distances(t_a),bins='auto')
#     plt.show()
    out_file=open("out-"+opt_string+str(node_density)+".csv","w")
    for i in range(len(dt)):
        print(str(dt[i])+", "+str(ot[i])+", "+str(oit[i])+", "+str(ib[i])+", "+str(pt[i]), file=out_file)

def main(variant):
    if variant=="aloha-small" or variant=="aloha-large":
        for nb in aloha_sim_nb_nodes:
            t_a=[]
            e_a=[]
            init_sim_homogeneous(nb,t_a,e_a)
            simulate(t_a,e_a)
            output_stats_homogeneous(t_a)
    else:
        t_a=[]
        e_a=[]
        init_sim_all_sf(t_a,e_a)
        simulate(t_a,e_a)
        output_stats_all_sf(t_a)
#         plot_toa_distances(t_a)
#         plot_sf_nb(t_a)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Simple LoRa Simulator')
    parser.add_argument("variant",choices=['large', 'medium', 'small', 'smallopt', 'mediumopt', 'largeopt', 'smalloptrel', 'mediumoptrel', 'largeoptrel', 'aloha-small', 'aloha-large'])
    parser.add_argument("--eqpwr", help='Somewhat equalize received pwr in SF7', dest='eqpwr',action='store_true')
    parser.add_argument("--co_sf_low", help='Co SF threshld from Croce et al.', dest='co_sf_low',action='store_true')
    parser.add_argument("--same_dc", help='Same DC at all SF', dest='same_dc',action='store_true')

    args = parser.parse_args()
    if args.variant=="large":
        R=8500
        node_density=5 # nodes/km2
        sf_radii = [3.09, 3.72, 4.48, 5.40, 6.30, 7.36]
    elif args.variant=="medium":
        R=6500
        node_density=20 # nodes/km2
        sf_radii = [2.23, 2.68, 3.23, 3.89, 4.54, 5.23]
    elif args.variant=="small":
        R=4000
        node_density=90 # nodes/km2
        sf_radii = [1.18, 1.43, 1.72, 2.07, 2.41, 2.82]
    elif args.variant=="smallopt":
        R=4500
        node_density=90 # nodes/km2
        sf_radii = [2.441893, 3.023491, 3.337609, 3.509860, 3.592097, 4.500]
        opt_string="o-"
    elif args.variant=="mediumopt":
        R=6000
        node_density=20 # nodes/km2
        sf_radii = [3.155450, 3.843545, 4.382818, 4.774180, 4.988129, 6.000]
        opt_string="o-"
    elif args.variant=="largeopt":
        R=7000
        node_density=5 # nodes/km2
        sf_radii = [3.342061, 4.037641, 4.763760, 5.466016, 5.938459, 7.000]
        opt_string="o-"
    elif args.variant=="smalloptrel":
        R=3000
        node_density=90 # nodes/km2
        sf_radii = [1.223472, 1.521895, 1.671742, 1.749120, 1.784959, 3.000]
        opt_string="o-"
    elif args.variant=="mediumoptrel":
        R=4000
        node_density=20 # nodes/km2
        sf_radii = [1.883562, 2.312865, 2.586188, 2.756895, 2.843840, 4.000]
        opt_string="o-"
    elif args.variant=="largeoptrel":
        R=4500
        node_density=5 # nodes/km2
        sf_radii = [2.133966, 2.587690, 3.000140, 3.344809, 3.551296, 4.500]
        opt_string="o-"
    elif args.variant=="aloha-small":
        R=2500
    elif args.variant=="aloha-large":
        R=7500
    else: print("What?");exit()

    sf7_eqpwr=args.eqpwr

    if args.co_sf_low:
        rej_below = {7:0, 8: 11, 9:13, 10: 17, 11: 20, 12:23}
        rej_above = {7: 8, 8:11, 9:13, 10:17, 11:20, 12:0}

    same_dc=args.same_dc

    main(args.variant)