
class TransmitEvent():
    """Transmission Event class"""
    def __init__(self,t,d,r,s):
        self.time=t
        self.duration=d
        self.distance=r
        self.sf=s
        
    def __str__(self):
        return str(self.time)+" "+str(self.duration)+" "

    time=0.0
    duration=0.0
    distance=0.0
    sf=7
    success=True

class EndOfTrans():
    """End of transmission Event class"""
    def __init__(self,t,ref):
        self.time=t
        self.ref=ref # references the start of trans event

class AnnotatedTransmitEvent(TransmitEvent):
    def __init__(self,t,d,r,s):
        TransmitEvent.__init__(self,t,d,r,s)
    # in dBm
    r_pwr=0
    # survives thermal noise
    decoded=1
    # co SF interference level (not in dB !)
    co_sf_interference_level=0
    # observed largescale fading in dB
    ss_fading=0
    # Nb harful collisions (above 6dB...) 0 = OK
    coll=0
    # Total Nb collisions (including not harmful)
    all_coll=0
    # Collisions from other SFs
    inter_sf_coll=0
    # From a SF below ?
    inter_sf_below=0
